# aw-cli

## 安装

```javascript
npm install aw-pro-cli -g
```
#### 介绍
```js
用于快速创建项目模板的cli(目前俩个模板是一个基于Vue的,一个基于Uniapp的，都是自搭的模板)
```
#### 使用

```shell
aw-cli create <your projectName>
```

