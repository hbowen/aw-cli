#!/usr/bin/env node

const program = require('commander')

const helpOptions = require('./lib/core/help')
const createCommands = require('./lib/core/create')

// 查看版本号
program.version(require('./package.json').version) 

// 帮助和查看可选项
helpOptions()

// 创建指令
createCommands()


program.parse(process.argv)

// console.log(program.opts().framework)
// console.log(program._optionValues.framework)

