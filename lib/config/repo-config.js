// const vue = 'direct:https://gitee.com/hbowen/management-template.git'
// const mini = 'direct:https://gitee.com/hbowen/mini-template.git'

const REPO = {
  mini: 'direct:https://gitee.com/hbowen/cli-template.git#小程序模板基础版',
  miniCli: 'direct:https://gitee.com/hbowen/cli-template.git#小程序模板Cli',
  vue: 'direct:https://gitee.com/hbowen/management-template.git',
  roullp:'direct:https://gitee.com/hbowen/cli-template.git#Roullp',
  learn:'direct:https://gitee.com/hbowen/cli-template.git#learn',
  learnUi:'direct:https://gitee.com/hbowen/cli-template.git#learn-ui',
  uniVue2TailwindVscodeTemplate:'direct:https://gitee.com/hbowen/cli-template.git#uni-vue2-tailwind-vscode-template-美化'
}

const selectRepo = (template) => {
  return REPO[template]
}

module.exports = { selectRepo }
