const program = require('commander')
const { createProjectAction, addComponentAction } = require('./action')
const createCommands = () => {
  program
    .command('create <project> [others...]')
    .description('clone a repository into a folder')
    .action(createProjectAction) //执行完create后操作

  program
    .command('addcpn <name>')
    .description('add vue component,例如: aw-cli addcpn HelloWord -d src/components')
    .action((name) => {
      addComponentAction(name, program.dest || 'src/components')
    })
}
module.exports = createCommands
