const inquirer = require('inquirer')
const Inquirer = () => {
  return inquirer.prompt([
    {
      name: 'template',
      type: 'list',
      message: '请选择项目模板：', 
      default: 'mini',
      choices: [
        { value: 'mini', name: 'Uni模板(基础版)' },
        { value: 'miniCli', name: 'Uni模板(cli版)' },
        { value: 'vue', name: 'Vue模板' },
        { value: 'roullp', name: 'Roullp模板' },
        { value: 'learn', name: 'learn模板' },
        { value: 'learnUi', name: 'learn-ui模板' },
        { value: 'uniVue2TailwindVscodeTemplate', name: 'UniAPP基于VUE2、Tailwind-美化模板' }
      ]
    }
  ])
}
module.exports = {
  Inquirer
}
