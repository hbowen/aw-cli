// 封装
const ora = require('ora')
const chalk = require('chalk')

const spinner = ora()
// #66c2cd
// const spinner = ora('Loading unicorns').start();
const start = (msg) => {
  spinner.text = chalk.blue(msg)
  spinner.start()
}
const success = (msg) => {
  spinner.text = chalk.green(msg)
  spinner.succeed()
}
const finish = (msg) => {
  spinner.stopAndPersist({
    symbol: chalk.green('√'),
    text: chalk.green(msg)
  })
}

const stop = () => {
  spinner.stop()
}

const error = (msg) => {
  spinner.fail(chalk.red(msg))
}

module.exports = {
  start,
  stop,
  finish,
  success,
  error
}

// // 使用
// const spinner from './spinner';

// spinner.start('Loading...');

// setTimeout(() => {
//   spinner.success('Load success');
// }, 2000);
